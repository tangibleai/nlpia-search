from django.shortcuts import render

from django.http import HttpResponseRedirect  # , HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse

from .models import SearchQuery, Domain


def index(request):
    return render(request, 'search/index.html', {})


def searchbox(request, corpus_id):
    domain = get_object_or_404(Domain, pk=corpus_id)
    try:
        selected_choice = domain.choice_set.get(pk=request.POST['choice'])
    except (KeyError, SearchQuery.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'search/detail.html', {
            'domain': domain,
            'error_message': "You didn't enter any search text!",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        #                                    polls:results,   args=(question.id,)
        return HttpResponseRedirect(reverse('search:results', args=(domain.id,)))
