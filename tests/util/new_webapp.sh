#!/usr/bin/env sh
# new_webapp.sh

# INSTALL PYTHON & POETRY
# sudo apt install python-is-python3 python3-dev python3
# curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

SCRIPT=$0
PROJECT=$1
APP=$2

# CREATE PROJECT
cd ~/code/tangibleai
conda activate py37 || conda create -y -n py37 python=3.7 && conda activate py37
poetry new $PROJECT
cd $PROJECT

# convert the `-`s in $PROJECT to `_`s
mkdir -p tests/util
cp $SCRIPT tests/util/
echo "import sys" > tests/util/clean_variable_name.py
echo "print(sys.argv[1].replace('-', '_'))" >> tests/util/clean_variable_name.py
MODULE=$(python tests/util/clean_variable_name.py $PROJECT)
rm -rf $MODULE

python -m venv venv
# workon py37

poetry add django
poetry add gunicorn
poetry run django-admin startproject $MODULE .

cp ../nlpia2/.gitignore .
git init .
mv README.rst README.md
echo "# Django skeleton app" >> README.md
sed -E s/'python = "\^[34][.][7891][012]?"'/'python = "^3.7"'/g -i pyproject.toml

python manage.py migrate
poetry run ./manage.py runserver

firefox http://localhost:8000/
python manage.py startapp $APP
