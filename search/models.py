from django.db import models


class SearchQuery(models.Model):
    text = models.CharField(blank=True, max_length=128,
                            help_text="e.g. who coined the term positive sum game")
    domain = models.ForeignKey('Domain', on_delete=models.PROTECT, null=True, blank=True)

    # https://timonweb.com/django/automatic-created-and-updated-datetime-fields-for-django-models/
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Domain(models.Model):
    name = models.CharField(blank=True, max_length=64,
                            help_text="e.g. Wikipedia")

    # https://timonweb.com/django/automatic-created-and-updated-datetime-fields-for-django-models/
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
