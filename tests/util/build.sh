#!/usr/bin/env bash
# exit on error
set -o errexit

# sudo and any system-level writes fail on Render
# apt-get update
# apt-get install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev liblzma-dev
# curl -O https://www.python.org/ftp/python/3.9.13/Python-3.9.13.tgz
# tar -xf Python-3.9.13.tgz
# cd Python-3.9.13
# make -j 1
# make altinstall

poetry env use $(which python3.9)

poetry install

python manage.py collectstatic --no-input

python manage.py migrate

